module.exports = function(app) {
    app.get("/registration",function(req, res) {
        res.send('Lista de pagamentos');
    });
    app.post("/registration/register",function (req,res) {
        var register = req.body;

        var plantingArray = [];

        plantingArray = register.planting;

        register.planting = plantingArray.toString();

        register.date_registration = new Date;
        console.log(register);

        req.assert("latitude", "latitude é obrigatória.").notEmpty();
        req.assert("longitude", "Longitude é obrigatório e deve ser um decimal.").notEmpty().isFloat();
        req.assert("owner", "Nome do proprietario é obrigatório e deve ter no minimo 3 caracteres").notEmpty().len(3,25);

        var errors = req.validationErrors();

        if (errors){
            console.log("Erros de validação encontrados");
            res.status(400).send(errors);
            return;
        }
        console.log('processando registro...');
        var connection = app.DAO.connectionFactory();
        var registerDao = new app.DAO.RegisterDAO(connection);

        registerDao.salva(register, function(exception, result){
            console.log('registro criado: ' + result);

            //res.location('/registration/register/' + result.insertId);

            //register.id = result.insertId;

            res.status(201).json(register);
    });
});
}
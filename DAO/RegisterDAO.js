function RegisterDAO(connection) {
    this._connection = connection;
}

RegisterDAO.prototype.salva = function(register,callback) {
    this._connection.query('INSERT INTO registration SET ?', register, callback);
}

RegisterDAO.prototype.lista = function(callback) {
    this._connection.query('select * from registration',callback);
}

RegisterDAO.prototype.buscaPorId = function (id,callback) {
    this._connection.query("select * from registration where id = ?",[id],callback);
}

module.exports = function(){
    return RegisterDAO;
};
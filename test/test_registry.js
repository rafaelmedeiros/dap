var should = require("should");
var request = require("request");
var chai = require("chai");
var expect = chai.expect;
var urlBase = "http://localhost:3000/registration/";
describe("Teste API de treinamento para Assert",function(){


    it("Successful farmer registration",function(done){

        request.post({url : urlBase + "register",
                      headers: {'content-type' : 'application/x-www-form-urlencoded'},
                      form: {owner: "Rafael Medeiros",
                          latitude:10343187,
                          longitude: 123213214,
                          height: "2 metros",
                          status: "plantio em 1 ano",
                          description: "aloaloaloalo",
                          profile: "dono",
                          planting: ["arroz","milho"],
                          address: "Rua abdon chianca 65"}},function(error, response,body){
                try{
                     var _body = JSON.parse(body);
                     console.log(_body);
                }
                catch(e){
                   var  _body = {};
                }
                console.log(response.toString());
                expect(response.statusCode).to.equal(201);


                done();
            });
    });

    it("Wrong farmer registration",function(done){

        request.post(
            {
                url : urlBase + "register",
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                form: {owner: "Rafael Medeiros",
                    latitude:10343187,
                    height: "2 metros",
                    status: "plantio em 1 ano",
                    description: "aloaloaloalo",
                    profile: "dono",
                    planting: "arroz",
                    address: "Rua abdon chianca 65"}
            },
            function(error, response, body){
                var _body = {};
                try{
                    _body = JSON.parse(body);
                }
                catch(e){
                    _body = {};
                }
                expect(response.statusCode).to.equal(400);

                done();
            }
        );
    });
});